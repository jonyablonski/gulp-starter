/**
 * Required Plugins
 */

var gulp = require('gulp');
var fileinclude = require('gulp-file-include');
var minifyHTML = require('gulp-minify-html');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var uncss = require('gulp-uncss');
var browserSync = require('browser-sync');
var jshint = require('gulp-jshint');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var minifyCSS = require('gulp-minify-css');
var imagemin = require('gulp-imagemin');
var rename = require("gulp-rename");
var notify = require('gulp-notify');
var cache = require('gulp-cache');
var del = require('del');
var runSequence = require('run-sequence');

/**
 * Paths
 */


/**
 * Development Tasks
 */

// Start browserSync server
gulp.task('browserSync', function() {
  browserSync({
    server: {
      baseDir: 'app'
    }
  })
});

// Build HTML
gulp.task('html', function() {
  return  gulp.src('./app/templates/*.tpl.html')
    .pipe(fileinclude())
    .pipe(rename({
      extname: ""
     }))
    .pipe(rename({
      extname: ".html"
     }))
    .pipe(gulp.dest('./app/'))
    .pipe(minifyHTML('conditionals: true, spare:true'))
    .pipe(gulp.dest('./dist/'))
    .pipe(notify({ message: 'HTML is built' }))
    .pipe(browserSync.stream())
    .pipe(browserSync.reload({
      stream: true
    }));
});

// Build Styles
gulp.task('styles', function() {
  return gulp.src('app/sass/main.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({ style: 'expanded', sourceComments: 'map', errLogToConsole: true}))
    .pipe(sourcemaps.write())
    .pipe(autoprefixer('last 2 version', "> 1%", 'ie 9'))
    .pipe(uncss({
        html: ['app/*.html']
    }))
    .pipe(gulp.dest('app/css/build/'))
    .pipe(minifyCSS())
    .pipe(gulp.dest('dist/css'))
    .pipe(notify({ message: 'Styles are built' }))
    .pipe(browserSync.reload({
      stream: true
    }));
});

// Lint Javascript
gulp.task('jshint',function(){
  gulp.src(['app/*.js'])
    .pipe(jshint('.jshintrc'))
    .pipe(jshint.reporter('jshint-stylish'))
    .pipe(notify({ message: 'Scripts are linted' }))
    .pipe(browserSync.reload({
      stream: true
    }));
});

// Build Javascript
gulp.task('scripts',function(){
  gulp.src(['app/js/**/*.js'])
    .pipe(sourcemaps.init())
    .pipe(concat('main.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('app/js/build/'))
    .pipe(uglify())
    .pipe(gulp.dest('dist/js/'))
    .pipe(notify({ message: 'Scripts are built' }))
    .pipe(browserSync.reload({
      stream: true
    }));
});

// Watchers
gulp.task('watch', function() {
  gulp.watch('app/sass/**/*.scss', ['styles']);
  gulp.watch('app/*.html', browserSync.reload);
  gulp.watch('app/js/**/*.js', browserSync.reload);
});

/**
 * Optimization Tasks
 */

// Optimizing Images
gulp.task('images', function() {
  return gulp.src('app/images/**/*.+(png|jpg|jpeg|gif|svg)')
  .pipe(cache(imagemin({
      interlaced: true,
    })))
  .pipe(gulp.dest('dist/images'))
  .pipe(notify({ message: 'Images are optimized' }))
});

// Copying fonts
gulp.task('fonts', function() {
  return gulp.src('app/fonts/**/*')
  .pipe(gulp.dest('dist/fonts'))
});

// Cleaning
gulp.task('clean', function(callback) {
  del('dist');
  return cache.clearAll(callback);
});

gulp.task('clean:dist', function(callback) {
  del(['dist/**/*', '!dist/images', '!dist/images/**/*'], callback)
});

/**
 * Build Sequences
 */

gulp.task('default', function(callback) {
  runSequence(['html', 'styles', 'jshint', 'scripts', 'browserSync', 'watch'],
    callback
  )
});

gulp.task('build', function(callback) {
  runSequence('clean:dist',
    ['html', 'styles', 'jshint', 'scripts', 'images', 'fonts'],
    callback
  )
});
